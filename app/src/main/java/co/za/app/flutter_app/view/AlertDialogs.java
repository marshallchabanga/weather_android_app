package co.za.app.flutter_app.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import co.za.app.flutter_app.R;
import co.za.app.flutter_app.adapters.FavouriteListAdapter;
import co.za.app.flutter_app.callbacks.OnAlertDialogClicked;
import co.za.app.flutter_app.callbacks.OnFavouriteSelected;
import co.za.app.flutter_app.models.Favourite;

import java.util.List;

public class AlertDialogs {


	public static AlertDialog createInformationDialog(Context context,
	                                                  String message,
	                                                  String titleMsg,
	                                                  final OnAlertDialogClicked onAlertDialogClicked) {

		final AlertDialog builder =
				new AlertDialog.Builder(context).create();
		LayoutInflater layoutInflater = (LayoutInflater.from(context));
		View view = layoutInflater.inflate(R.layout.dialog_confirmation, null);
		Button cancel = view.findViewById(R.id.btn_cancel);
		Button confirm = view.findViewById(R.id.btn_confirm);
		TextView txtDetails = view.findViewById(R.id.txt_details);
		TextView txtTitle = view.findViewById(R.id.txtTitle);

		if (!TextUtils.isEmpty(txtTitle.getText().toString())) {
			txtTitle.setText(titleMsg);

		}
		txtDetails.setText("" + message);
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				builder.dismiss();
				onAlertDialogClicked.onYesClicked(false);
			}
		});

		confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				builder.dismiss();

				onAlertDialogClicked.onYesClicked(true);


			}
		});
		builder.setView(view);
		return builder;
	}

	public static AlertDialog favouritesDialog(Context context,
	                                           List<Favourite> favourites,
	                                           final OnFavouriteSelected onAlertDialogClicked
	                                           ) {

		 FavouriteListAdapter   favouriteListAdapter=new FavouriteListAdapter(favourites,context);
		final AlertDialog builder =
				new AlertDialog.Builder(context).create();
		LayoutInflater layoutInflater = (LayoutInflater.from(context));
		View view = layoutInflater.inflate(R.layout.favourites_list, null);
		RecyclerView recyclerView = view.findViewById(R.id.rcvFavourites);
		recyclerView.setLayoutManager(new LinearLayoutManager(context));
		recyclerView.setAdapter(favouriteListAdapter);
		favouriteListAdapter.setOnItemClickListener(new FavouriteListAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, Favourite obj, int position) {
				builder.dismiss();

				onAlertDialogClicked.favouriteSelected(obj);

			}
		});
		builder.setView(view);
		return builder;
	}

}
