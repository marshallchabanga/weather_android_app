package co.za.app.flutter_app.dtos;

public class Coord {
	double lat;
	double lon;

	public double getLat() {

		return lat;
	}

	public void setLat(double lat) {

		this.lat = lat;
	}

	public double getLon() {

		return lon;
	}

	public void setLon(double lon) {

		this.lon = lon;
	}
}