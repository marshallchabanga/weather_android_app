package co.za.app.flutter_app.db;

import android.content.Context;
import co.za.app.flutter_app.models.Favourite;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmManager {

	private Context context;

	private Realm realm;

	public RealmManager(Context context) {

		this.context = context;
		realm = Realm.getDefaultInstance();

	}


	public boolean saveFavourite(Favourite favourite) {


		realm.beginTransaction();
		realm.copyToRealmOrUpdate(favourite);
		realm.commitTransaction();
		return true;
	}
	public RealmResults<Favourite> getAllfavourites(){
		RealmResults<Favourite> realmResults = realm.where(Favourite.class)
				.findAll();
		realmResults.sort("name", Sort.DESCENDING);
		return  realmResults;
	}

}
