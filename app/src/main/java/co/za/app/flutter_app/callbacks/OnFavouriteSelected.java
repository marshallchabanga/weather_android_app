package co.za.app.flutter_app.callbacks;

import co.za.app.flutter_app.models.Favourite;

public interface OnFavouriteSelected {
	void favouriteSelected(Favourite favourite);
}
