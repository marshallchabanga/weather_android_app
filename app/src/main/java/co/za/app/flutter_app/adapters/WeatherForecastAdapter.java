package co.za.app.flutter_app.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import co.za.app.flutter_app.R;
import co.za.app.flutter_app.dtos.ListData;
import com.bumptech.glide.Glide;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.WeatherForecastViewHolder> {

	private List<ListData> listData;
	private Context        context;
	private static DecimalFormat df2 = new DecimalFormat("#.##");

	public WeatherForecastAdapter(List<ListData> listData, Context context) {

		this.listData = listData;
		this.context = context;
	}

	@NonNull
	@Override
	public WeatherForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.layout_weather_forecast, parent, false);

		return new WeatherForecastViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull WeatherForecastViewHolder holder, int position) {

		ListData data= listData.get(position);

		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("EEE , HH:ss");
		Date date=new Date(data.getDt()* 1000);
		String format = simpleDateFormat.format(date);

		holder.day.setText(format);

		double tmp = data.getMain().getTemp();
		double temp = tmp - 273.15;


		df2.setRoundingMode(RoundingMode.UP);

		holder.temperature.setText(df2.format(temp)+"°");

		String icon = data.getWeather().get(0).getIcon();

		Glide.with(context).load("http://openweathermap.org/img/w/"+icon+".png").into(holder.weatherIcon);
	}

	@Override
	public int getItemCount() {

		return listData.size();
	}

	public class WeatherForecastViewHolder extends RecyclerView.ViewHolder {

		private TextView  day;
		private ImageView weatherIcon;
		private TextView  temperature;


		public WeatherForecastViewHolder(View view) {

			super(view);
			day = view.findViewById(R.id.txtDay);
			temperature = view.findViewById(R.id.txtTemp);
			weatherIcon = view.findViewById(R.id.imgIcon);

		}

	}

	public void setListData(List<ListData> listData) {

		this.listData.clear();
		this.listData = listData;
		notifyDataSetChanged();
	}


}
