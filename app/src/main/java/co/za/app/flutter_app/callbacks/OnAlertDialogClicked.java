package co.za.app.flutter_app.callbacks;

public interface OnAlertDialogClicked {

	void onYesClicked(boolean isYes);
}
