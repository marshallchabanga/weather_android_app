package co.za.app.flutter_app.dtos;

public class Wind {
	Object speed;
	int deg;

	public Object getSpeed() {

		return speed;
	}

	public void setSpeed(Object speed) {

		this.speed = speed;
	}

	public int getDeg() {

		return deg;
	}

	public void setDeg(int deg) {

		this.deg = deg;
	}
}
