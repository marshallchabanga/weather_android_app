package co.za.app.flutter_app.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Favourite extends RealmObject {

	@PrimaryKey
	String id;
	String name;
	double latitude;
	double longitude;

	public Favourite() {

	}

	public Favourite(String id, String name, double latitude, double longitude) {

		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public double getLatitude() {

		return latitude;
	}

	public void setLatitude(double latitude) {

		this.latitude = latitude;
	}

	public double getLongitude() {

		return longitude;
	}

	public void setLongitude(double longitude) {

		this.longitude = longitude;
	}
}
