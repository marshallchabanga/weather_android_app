package co.za.app.flutter_app.dtos;

import java.util.List;

public class ListData {
	long           dt;
	Main          main;
	List<Weather> weather;
	Clouds        clouds;
	Wind          wind;
	Sys           sys;
	String        dtTxt;
	Rain          rain;

	public long getDt() {

		return dt;
	}

	public void setDt(int dt) {

		this.dt = dt;
	}

	public Main getMain() {

		return main;
	}

	public void setMain(Main main) {

		this.main = main;
	}

	public List<Weather> getWeather() {

		return weather;
	}

	public void setWeather(List<Weather> weather) {

		this.weather = weather;
	}

	public Clouds getClouds() {

		return clouds;
	}

	public void setClouds(Clouds clouds) {

		this.clouds = clouds;
	}

	public Wind getWind() {

		return wind;
	}

	public void setWind(Wind wind) {

		this.wind = wind;
	}

	public Sys getSys() {

		return sys;
	}

	public void setSys(Sys sys) {

		this.sys = sys;
	}

	public String getDtTxt() {

		return dtTxt;
	}

	public void setDtTxt(String dtTxt) {

		this.dtTxt = dtTxt;
	}

	public Rain getRain() {

		return rain;
	}

	public void setRain(Rain rain) {

		this.rain = rain;
	}
}
