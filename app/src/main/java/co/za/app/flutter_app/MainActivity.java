package co.za.app.flutter_app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import co.za.app.flutter_app.adapters.WeatherForecastAdapter;
import co.za.app.flutter_app.api.WeatherClient;
import co.za.app.flutter_app.callbacks.OnAlertDialogClicked;
import co.za.app.flutter_app.callbacks.OnFavouriteSelected;
import co.za.app.flutter_app.db.RealmManager;
import co.za.app.flutter_app.dtos.CurrentWeather;
import co.za.app.flutter_app.dtos.DaysForecast;
import co.za.app.flutter_app.dtos.ListData;
import co.za.app.flutter_app.models.Favourite;
import co.za.app.flutter_app.models.LocationPref;
import co.za.app.flutter_app.utils.Preference;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.*;

import static co.za.app.flutter_app.utils.Utils.*;
import static co.za.app.flutter_app.view.AlertDialogs.createInformationDialog;
import static co.za.app.flutter_app.view.AlertDialogs.favouritesDialog;


public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {

	private static final String TAG                       = MainActivity.class.getSimpleName();
	private              int    AUTOCOMPLETE_REQUEST_CODE = 90;
	private              int    FAVOURITE_REQUEST_CODE    = 91;

	private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

	private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

	private static final int REQUEST_CHECK_SETTINGS = 100;

	private FusedLocationProviderClient mFusedLocationClient;
	private SettingsClient              mSettingsClient;
	private LocationRequest             mLocationRequest;
	private LocationSettingsRequest     mLocationSettingsRequest;
	private LocationCallback            mLocationCallback;
	private Location                    mCurrentLocation;

	private Boolean      mRequestingLocationUpdates;
	private PlacesClient placesClient;

	private RecyclerView recyclerView;
	private TextView     title;
	private TextView     location;
	private TextView     max, min, current;
	private ImageView              backgroundImage;
	private LinearLayout           parentLinear;
	private Toolbar                toolbar;
	private WeatherForecastAdapter weatherForecastAdapter;

	private        List<ListData>  listData   = new ArrayList<>();
	private        List<Favourite> favourites = new ArrayList<>();
	private static DecimalFormat   df2        = new DecimalFormat("#.##");
	private        Preference      preference;
	private        boolean         isRefresh;

	private void init() {

		mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
		mSettingsClient = LocationServices.getSettingsClient(this);

		mLocationCallback = new LocationCallback() {
			@Override
			public void onLocationResult(LocationResult locationResult) {

				super.onLocationResult(locationResult);
				// location is received
				mCurrentLocation = locationResult.getLastLocation();
					getCurrentLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
				getWeatherForecast(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
				stopLocationUpdates();

			}
		};

		mRequestingLocationUpdates = false;
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
		builder.addLocationRequest(mLocationRequest);
		mLocationSettingsRequest = builder.build();

		// Initialize Places.
		Places.initialize(getApplicationContext(), "AIzaSyCsF5IIXuppjvVFwTReB7LG7b1MiwAIYo0");

		// Create a new Places client instance.
		placesClient = Places.createClient(this);


	}

	@Override
	public void onBackPressed() {

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		recyclerView = findViewById(R.id.rcvForecast);
		NavigationView navigationView = findViewById(R.id.nav_view);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();
		navigationView.setNavigationItemSelectedListener(this);
		init();
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		weatherForecastAdapter = new WeatherForecastAdapter(listData, this);
		recyclerView.setAdapter(weatherForecastAdapter);
		title = findViewById(R.id.txtCTitle);
		location = findViewById(R.id.txtLocation);
		max = findViewById(R.id.txtMax);
		min = findViewById(R.id.txtMin);
		current = findViewById(R.id.txtCurrent);
		backgroundImage = findViewById(R.id.imgCurrentWeather);
		parentLinear = findViewById(R.id.lnrParent);
		preference = new Preference(this);


		if (mRequestingLocationUpdates && checkPermissions()) {
			startLocationUpdates();
		}
		startLocation();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				Place place = Autocomplete.getPlaceFromIntent(data);
				searchLocation(place.getLatLng().latitude, place.getLatLng().longitude);

			} else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
				Status status = Autocomplete.getStatusFromIntent(data);
				Log.i(TAG, status.getStatusMessage());
			} else if (resultCode == RESULT_CANCELED) {
				// The user canceled the operation.
			}
		} else if (requestCode == FAVOURITE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				Place place = Autocomplete.getPlaceFromIntent(data);

				saveFavouriteLocation(place);

			} else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
				Status status = Autocomplete.getStatusFromIntent(data);
				Log.i(TAG, status.getStatusMessage());
			} else if (resultCode == RESULT_CANCELED) {
				// The user canceled the operation.
			}
		}
	}

	private void openSettings() {

		Intent intent = new Intent();
		intent.setAction(
				Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		Uri uri = Uri.fromParts("package",
				BuildConfig.APPLICATION_ID, null);
		intent.setData(uri);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void searchLocation() {

		List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
		// Start the autocomplete intent.
		Intent intent = new Autocomplete.IntentBuilder(
				AutocompleteActivityMode.FULLSCREEN, fields)
				.build(this);
		startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
	}

	private void openLocationDialog() {

		List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
		// Start the autocomplete intent.
		Intent intent = new Autocomplete.IntentBuilder(
				AutocompleteActivityMode.FULLSCREEN, fields)
				.build(this);
		startActivityForResult(intent, FAVOURITE_REQUEST_CODE);
	}

	private void startLocationUpdates() {


		if (!isRefresh && preference.getCurrentWeather() != null &&
				preference.getDaysForecast() != null &&
				preference.getLocationPref() != null) {
			LocationPref locationPref = preference.getLocationPref();
			updateCurrentWeatherUI(preference.getCurrentWeather(), locationPref.getLat(), locationPref.getLon());
			weatherForecastAdapter.setListData(preference.getDaysForecast().getList());
			return;
		}

		mSettingsClient
				.checkLocationSettings(mLocationSettingsRequest)
				.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
					@SuppressLint("MissingPermission")
					@Override
					public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

						Log.i(TAG, "All location settings are satisfied.");

						Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

						//noinspection MissingPermission
						mFusedLocationClient.requestLocationUpdates(mLocationRequest,
								mLocationCallback, Looper.myLooper());

						//todo update ui
					}
				})
				.addOnFailureListener(this, new OnFailureListener() {
					@Override
					public void onFailure(@NonNull Exception e) {

						int statusCode = ((ApiException) e).getStatusCode();
						switch (statusCode) {
							case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
								Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
										"location settings ");
								try {
									// Show the dialog by calling startResolutionForResult(), and check the
									// result in onActivityResult().
									ResolvableApiException rae = (ResolvableApiException) e;
									rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
								} catch (IntentSender.SendIntentException sie) {
									Log.i(TAG, "PendingIntent unable to execute request.");
								}
								break;
							case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
								String errorMessage = "Location settings are inadequate, and cannot be " +
										"fixed here. Fix in Settings.";
								Log.e(TAG, errorMessage);

								Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
						}

					}
				});
	}

	public void stopLocationUpdates() {
		// Removing location updates
		mRequestingLocationUpdates = false;
		mFusedLocationClient
				.removeLocationUpdates(mLocationCallback)
				.addOnCompleteListener(this, new OnCompleteListener<Void>() {
					@Override
					public void onComplete(@NonNull Task<Void> task) {

						Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
					}
				});
	}

	private void startLocation() {
		// Requesting ACCESS_FINE_LOCATION using Dexter library
		Dexter.withActivity(this)
				.withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
				.withListener(new PermissionListener() {
					@Override
					public void onPermissionGranted(PermissionGrantedResponse response) {

						mRequestingLocationUpdates = true;
						startLocationUpdates();
					}

					@Override
					public void onPermissionDenied(PermissionDeniedResponse response) {

						if (response.isPermanentlyDenied()) {

							openSettings();
						}
					}

					@Override
					public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
					                                               PermissionToken token) {

						token.continuePermissionRequest();
					}
				}).check();
	}

	@Override
	public void onResume() {

		super.onResume();


	}

	private boolean checkPermissions() {

		int permissionState = ActivityCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_FINE_LOCATION);
		return permissionState == PackageManager.PERMISSION_GRANTED;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.nav_search) {

			searchLocation();

		} else if (id == R.id.nav_save) {
			openLocationDialog();
		} else if (id == R.id.nav_favourites) {
			showFavourites();
		}

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	private void showFavourites() {

		RealmManager realmManager = new RealmManager(this);
		RealmResults<Favourite> allFavourites = realmManager.getAllfavourites();

		if (allFavourites != null && !allFavourites.isEmpty()) {
			Iterator<Favourite> iterator = allFavourites.iterator();
			favourites.clear();
			while ((iterator.hasNext())) {
				Favourite favourite = iterator.next();
				favourites.add(favourite);

			}

			favouritesDialog(this,
					favourites,
					new OnFavouriteSelected() {
						@Override
						public void favouriteSelected(Favourite favourite) {

							searchLocation(favourite.getLatitude(), favourite.getLongitude());

						}
					}).show();

		} else {
			Toast.makeText(this, "Empty Favourite List", Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			isRefresh = true;
			startLocationUpdates();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}


	private void searchLocation(double latitude, double longitude) {

		getCurrentLocation(latitude, longitude);
		getWeatherForecast(latitude, longitude);
	}

	private void saveFavouriteLocation(final Place place) {
		//show confirmation dialog
		//create Favourite Object
		createInformationDialog(this, "Are you sure you want to add " + place.getName() + " to favourite", "New " +
						"Favourite",
				new OnAlertDialogClicked() {
					@Override
					public void onYesClicked(boolean isYes) {

						if (isYes) {
							Favourite favourite = new Favourite();
							favourite.setId(UUID.randomUUID().toString());
							favourite.setLatitude(place.getLatLng().latitude);
							favourite.setLongitude(place.getLatLng().longitude);
							favourite.setName(place.getName());


							RealmManager realmManager = new RealmManager(MainActivity.this);
							boolean saveFavourite = realmManager.saveFavourite(favourite);

							if (saveFavourite) {
								Toast.makeText(MainActivity.this, "" + favourite.getName() + " was saved successful",
										Toast.LENGTH_SHORT).show();
							}
						}
					}
				}).show();

	}


	private void getCurrentLocation(final double latitude, final double longitude) {

		WeatherClient.WeatherContract weatherClient = WeatherClient.getWeatherContract();
		weatherClient.currentWeather(latitude, longitude, "d528bf7dca606449c6df6b4ccd073ca9").enqueue(new Callback<CurrentWeather>() {
			@Override
			public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {

				if (response.code() == 200) {
					updateCurrentWeatherUI(response.body(), latitude, longitude);

					preference.saveCurrentWeather(response.body());

					LocationPref locationPref = new LocationPref(latitude, longitude);
					preference.saveLocationPref(locationPref);
				}
			}

			@Override
			public void onFailure(Call<CurrentWeather> call, Throwable t) {

				Log.e(TAG, t + "");
				Toast.makeText(MainActivity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


			}
		});

	}

	private void getWeatherForecast(double latitude, double longitude) {

		WeatherClient.WeatherContract weatherClient = WeatherClient.getWeatherContract();
		weatherClient.forecastWeather(latitude, longitude, "d528bf7dca606449c6df6b4ccd073ca9").enqueue(new Callback<DaysForecast>() {
			@Override
			public void onResponse(Call<DaysForecast> call, Response<DaysForecast> response) {

				if (response.code() == 200) {
					DaysForecast daysForecast = response.body();
					weatherForecastAdapter.setListData(daysForecast.getList());
					preference.saveForecastWeather(daysForecast);
				}
			}

			@Override
			public void onFailure(Call<DaysForecast> call, Throwable t) {

				Log.e(TAG, t + "");
				Toast.makeText(MainActivity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


			}
		});

	}

	private void updateCurrentWeatherUI(CurrentWeather currentWeather, double lat, double lon) {

		double tmp = currentWeather.getMain().getTemp();
		double temp = tmp - 273.15;
		String title = currentWeather.getWeather().get(0).getMain();
		df2.setRoundingMode(RoundingMode.UP);
		this.title.setTextColor(Color.WHITE);
		this.title.setText(df2.format(temp) + "°\n" + title);
		int image = buildTopImage(currentWeather, this);
		backgroundImage.setImageResource(image);
		int backgroundColor = backgroundColor(currentWeather);
		parentLinear.setBackgroundColor(backgroundColor);
		toolbar.setBackgroundColor(backgroundColor);
		String completeAddressString = getCompleteAddressString(this, lat,
				lon);
		location.setText(completeAddressString);
		minValue(currentWeather);
		maxValue(currentWeather);
		currentValue(currentWeather);

	}


	private void minValue(CurrentWeather currentWeather) {

		double tmp = currentWeather.getMain().getTemp_min();
		double temp = tmp - 273.15;
		df2.setRoundingMode(RoundingMode.UP);
		this.min.setText(df2.format(temp) + "°\nmin");
	}

	private void maxValue(CurrentWeather currentWeather) {

		double tmp = currentWeather.getMain().getTemp_max();
		double temp = tmp - 273.15;
		df2.setRoundingMode(RoundingMode.UP);
		this.max.setText(df2.format(temp) + "°\nmax");
	}

	private void currentValue(CurrentWeather currentWeather) {

		double tmp = currentWeather.getMain().getTemp();
		double temp = tmp - 273.15;
		df2.setRoundingMode(RoundingMode.UP);
		this.current.setText(df2.format(temp) + "°\nCurrent");
	}
}
